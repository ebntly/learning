---
layout: feature
title: Systems
tags: [systems, group]
category: group
subnav: false
features:
  -
    title: "CompTIA A+"
    url: /sys/aplus
    description: >-
        The CompTIA A+ certification is a entry level IT certification, consisting
        of two exams covering topics found in tech support and IT operational
        roles. Topics covered include: hardware, software, operating systems,
        mobile devices, security, troubleshooting, and operational procedures.
---



