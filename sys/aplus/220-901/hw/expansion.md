---
title: PC expansion slots
tags: [system, a+, computer, pc, server, hardware]
categories: [comptia, systems, aplus]
tag: 
    text: Install and configure PC expansion cards.
    cite: CompTIA A+

todo: 
    - Add architecture layout from messer motherboard expansion slots
---
Expansion slots add additional capabilities.

The expansion bus is measured in bandwidth (width), how much traffic can be pushed through a connection. The clock speed of the bus is how how fast the bus is moving data. Every bus has its own clock and is able to send information at a certain rate of speed. We measure the speed as the number of cycles performed per second, or Hz. 1 million cycles per second is a MHz. 1Ghz is 1000MHz. The faster the clock is the more information we can transfer. Clock speed does not necessarily equal transfer rate. DDR3 SDRAM can transfer 64 times the memory clock speed.

## Sound cards
## Video cards
## Network cards
## USB cards
## Firewire cards
## Thunderbolt cards
## Storage cards
## Modem cards
## Wireless/cellular cards
## TV tuner cards
## Video capture cards
## Riser cards
