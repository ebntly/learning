---
title: Storage devices
tags: [system, a+, computer, pc, server, hardware]
categories: [comptia, systems, aplus]
tag: 
    text: Install and configure storage devices and use appropriate media.
    cite: CompTIA A+
---

There are numerous storage forms. Our concerns for which storage form we'd use for a situation, we would want to look at a few things, such as cost, performance, lifetime, security, and size.

**Sizes**
Size in terms of storage is measured in bits and bytes.

| | Size |
|---|---|
| Bit (b) | 1/0 |
| Byte (B) | 8 bits |
| Kilobyte (KB) | 1024 B |
| Megabyte (MB) | 1024 KB |
| Gigabyte (GB) | 1024 MB |
| Terabyte (TB) | 1024 GB |
| Petabyte (PB) | 1024 TB |
| Exabyte (EB) | 1024 PB |

<!-- @TODO: Document sizes and history-->

## Optical drives
Uses a laser to read/write on the surface of the media
### CD-ROM/CD-RW
Compact disk - Read Only Memory/Compact disk - Rewritable
### DVD-ROM/DVD-RW/DVD-RW DL
DL stands for dual layer, which allows you to write twice the data than non dual layer disks.
### Blu-ray
### BD-R
### BD-RE
## Magnetic hard disk drives
### 5400 rpm
### 7200 rpm
### 10,000 rpm
## Hot swappable drives
## Solid state/flash drives
### Compact flash
### SD
### MicroSD
### MiniSD
### xD
### SSD
### Hybrid
### eMMC
## RAID types
### 0
### 1
### 5
### 10
## Tape drive
## Media capacity
### CD
### CD-RW
### DVD-RW
### DVD
### Blu-ray
### Tape
### DVD DL
