---
title: Storage devices
tags: [system, a+, computer, pc, server, hardware]
categories: [comptia, systems, aplus]
tag: 
    text: Install and configure storage devices and use appropriate media.
    cite: CompTIA A+
---

## Optical drives
### CD-ROM/CD-RW
### DVD-ROM/DVD-RW/DVD-RW DL
### Blu-ray
### BD-R
### BD-RE
## Magnetic hard disk drives
### 5400 rpm
### 7200 rpm
### 10,000 rpm
## Hot swappable drives
## Solid state/flash drives
### Compact flash
### SD
### MicroSD
### MiniSD
### xD
### SSD
### Hybrid
### eMMC
## RAID types
### 0
### 1
### 5
### 10
## Tape drive
## Media capacity
### CD
### CD-RW
### DVD-RW
### DVD
### Blu-ray
### Tape
### DVD DL
