---
title: PC connection interfaces
tags: [system, a+, computer, pc, server, hardware]
categories: [comptia, systems, aplus]
tag: 
    text: Compare and contrast various PC connection interfaces, their characteristics and purpose.
    cite: CompTIA A+
---

## Physical connections
### USB 1.1 vs. 2.0 vs. 3.0
#### Connector types: A, B, mini, micro
### Firewire 400 vs. Firewire 800
### SATA1 vs. SATA2 vs. SATA3, eSATA
### Other connector types
#### VGA
#### HDMI
#### DVI
#### Audio
##### Analog
##### Digital (Optical connector)
#### RJ-45
#### RJ-11
#### Thunderbolt
## Wireless connections
### Bluetooth
### RF
### IR
### NFC
## Characteristics
### Analog
### Digital
### Distance limitations
### Data transfer speeds
### Quality
### Frequencies
