---
title: Hardware
tags: [system, a+, computer, pc, server, hardware]
categories: [comptia, systems, aplus]
---

System hardware refers to the physical component which comprise the 
system. This is where computing becomes glamorous, with regard to largest hard drive, most memory, fastest CPU, etc... Most of the hardware components in a computer are modular, meaning they can be replaced by another hardware component that does the same function. Another term for a replaceable, modular hardware component is a field replaceable unit. The components of a computer can really be categorized into three main components: the motherboard, the processor, and memory.

Hardware failures can be caused by loose connections, physical or electrical damage, or device incompatibility.

Below are the CompTIA A+ 220 exam objectives for hardware:

* [BIOS/UEFI](./bios)