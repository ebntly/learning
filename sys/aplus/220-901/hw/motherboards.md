---
title: Motherboards
tags: [system, a+, computer, pc, server, hardware]
categories: [comptia, systems, aplus]
tag: 
    text: Explain the importance of motherboard components, their purpose and properties.
    cite: CompTIA A+
resources: [cbtnaplus901,profaplus901,pvaplus]
---
There are several characteristics that go into determining the motherboard you are working with:

* Physical size - case sizing
* Basic layout - room for small changes
* Standard connectors - power is the same regardless of the motherboard.
* Airflow - becoming increasinglt important.

There are over 40 different types of motherboards categorized.
## Sizes

### ATX
*Advanced Technology eXtended* is the basis for mid to full size desktop systems. It was standardized by Intel in 1995. It has dominated computer designs since the late 90s and replaced the AT and Baby-AT form factors from the mid 80s for the IBM PC AT and its rivals. ATX motherboards have an integrated port cluster (I/O backplate) on the left rear of the board and expansion slots that run parallel to the short side of the motherboard.  It is around 12 inches wide and about 9.6 inches deep. It can support up to 7 expansion slots. RAM slots are usually perpendicular to the expansion slots. It will have a 20-pin power connector, or a 24 pin power connector. There may be an additional 4 or 8 pin power conenctor. It is still very common and still manufactured today.

### Micro-ATX
Also from the ATX family, the microATX (mATX, uATX) is a smaller form factor about 9.6 inches square and can support up to 4 expansion slots. It is usually used in a mini-tower. It is backwards compatibile with similar mounting points and power as the ATX mother board. Very popular and still actively manufactured today.

### Mini-ITX
Mini-ITX is 6.7 inches square abd has been adopted by many vendors to use with AMD and Intel processors, which may be soldered in place or socketed. Original designs featured a single PCI expansion slot, but most recent systems include a PCIe x1 or x16 expansion slot. A Mini-ITX board can usually fit in a case made for the ATX family and uses a similar port cluster; however, Mini-ITX boards are used in small form factor PCs and single purpose computing, and home theater applications. Origianlly used passive cooling, but newer version use active cooling due to the more powerful CPU. Newer boards come with a PCIe x16 slot.

### ITX
*Information Technology eXtended* (ITX) was originally developed by VIA Technologies in 2001 for its low power x86 C3 processors. The ITX motherboard was quickly superseded by its small Mini-ITX form factor. It is a series of low-power motherboards.

4 types: Mini-ITX, Nano-ITX, Pico-ITX, Mobile-ITX.

Nano-ITX in 2005 is 4.7 inches squared consumes little power and is used for automotive, set-top boxes, personal video recorders, and some video centers.
Pico-ITX (2007) is 1/2 the area of the Nan-ITX at 3.9 in by 2.8 in and uses powerful processors, RAM, and active cooling, but is very small and used in ultra mobile PCs (UMPCs).
Mobile-ITX is 60mm square with no ports, and may require a secondary I/O board depending on the type of system. It is used in UMPCs and smartphones.

## Expansion slots
Mother boards use expansion slots to provide support for additional I/O devices and high-speed video/graphics cards. Most commonly on recent systems, they include Peripheral Component Interconnect (PCI) and PCI Express (PCIe).

| Slot Type | Performance | Uses |
|-----------|-------------|------|
| PCI 32-bit, 33MHz | 133MBps | video, networ, mass storage (SATA/RAID), sound card |
| PCI 32-bit, 66MHz | 266MBps | network, mass storage (workstation/server) |
| PCI 64-bit, 33MHz | 266 MBps | network, mass storage (workstation/server) |
| PCI 64-bit, 66MHz | 533 MBps | network, mass storage (workstation/server) |
| PCI-X, 66MHz | 533MBps | network, mass storage (workstation/server) |
| PCI-X, 133MHz | 1066MBps | network, mass storage (workstation/server) |
| PCI-X 2.0, 266MHz | 2133MBps | network, mass storage (workstation/server) |
| PCI-X 2.0, 533MHz | 4266MBps | network, mass storage (workstation/server) |
| *PCIe x1 v1 | 500MBps | network, I/O |
| *PCIe x4 v1 | 2000MBps | network |
| *PCIe x8 v1 | 4000MBps | multi-GPU video secondary card |
| *PCIe x16 v1 | 8000MBps | video primary and secondary cards |
| *PCIe x1 v2 | 1000MBps | network, I/O |
| *PCIe x4 v2 | 4000MBps | network |
| *PCIe x8 v2 | 8000MBps | video secondary card |
| *PCIe x16 v2 | 16000MBps | video primary and secondary cards |
| *PCIe x1 v3 | 2000MBps | network, I/O |
| *PCIe x4 v3 | 8000MBps | network |
| *PCIe x8 v3 | 16000MBps | video secondary card |
| *PCIe x16 v3 | 32000MBps | video primary and secondary cards |

> \* Bi-directional data rates (full duplex); Uni-directional rates are 1/2 of values listed

### PCI
*Peripheral Component Interconnect* slots were developed in 1992 and can be used with a number of add-on cards such as networking, video, audio, I/O, and storage host adapters for SATA drives. There are several types of PCI slots, but common in desktop computers is the 32-bit slot running at 33MHz. There are PCI slots available in the 66MHz and 64-bit version. 
It communicates in a parellel mode. Parellel communication means that for a single transfer all the bits are transferred at the same time, so we will have to communicate across multiple lane simultaneously, so we have 32 connections from the I/O controller to the PCI expansion slot for 32-bit PCI buses, and 64 connections for 64-bit bus. 

Throughput varies on bus version.

> See photo at https://en.wikipedia.org/wiki/File:PCI_Keying.png.

### PCI-X
*PCI eXtended* is a faster version of 64-bit PCI, running at 133MHz, and typically used in servers. It also supports PCI cards as well, as it uses the same connectos as 64-bit PCI slots. A PCI-X bus supports 2 PCI-X slots, but if you install a PCI-X card into a PCI-X slot on the same bus as a PCI card, the PCI-X card runs at the same speed as the PCI card. PCI-X2 supports 266MHz and 533MHz speeds; however, both types of PCI-X have been replaced by PCIe. Designed for servers, and built to handle higher-speed network and storage, it has higher bandwidth at 4x the clock speed and 1064MBps throughput.

### PCIe
*PCI Express* began to replace both PCI, PCI-X and AGP slots in systems starting in 2005. They are available in 5 types: x1, x4, x8, x16, x32, where x is "by". The x refers to an I/O lane, and the most common at x1, x4, and x16. Cards of lower I/O lanes can be used in those of higher I/O lanes. (I.E. 1x can be used in any, 4x can be used in x8 and x16, and 8x can be used in x16.) Some motherboards may have 2 or more x16 slots, but the additional slots may only support x4 or x8.

It communicates serially, with unidirectionall serial lines. It send a single bit from one side to another. It is faster as slower devices don't slow down everyone. It is full dpuplex, which means there is communication back and forth simultaneously. x1 has one lane to and one lane from the memory controller hub. x4 has 4 lanes to and 4 lanes from the memory controller.

There are different versions of PCIe with each iteration providing an increase in speed..

* v1.x: 250MBps
* v2.x: 500MBps
* v3.0: ~1GBps
* v4.0 :~2GBps<!--confirm-->



### miniPCI
MiniPCI and MiniPCIe are reduced-sizes of the PCI and PCIe standards and are used in laptops. There is also a miniPCI Express card. Can be used for Wifi adapters or mobile broadband connections.

## RAM slots
Modern motherboards, with the exception of some Mini-ITX and smaller boards, have 2 or more memory slots. At least one slot must contain a memory module or the system will be unable to start. They vary in design depending on the type of memory the system supports. Older systems that use SDRAM have three section memory slots designed for 168-pin memory modules. DDR DSRAM systems use 2 section memory modules slots designed for 184-pin memory modules. Systems that use DDR2 SDRAM have 2 section memory slots designed for 240-pin memory modules. DDR3 SDRAM also uses 2 section 240-pin memory slots, but the arrangement of the pins and eying of the slot are different than DDR2. DDR2 and DDR3 modules are not interchangeable. Some systems are now available with DDR4 memory. It uses 2 section memory with 288-pin memory slot. It uses 1.2V DC and has clock speeds of 1600 to 3200MHz.

## CPU sockets
*Central Processing Unit* ATX, microATX, and some Mini-ITX motherboards use various types of CPU sockets according to the CPU brand and family the board and chipset are designed to accommodate. Some Mini-ITX motherboard use soldered-in-place processors that cannot be interchanged. Most of these motherboards are intended for embedded systems.

## Chipsets
Just as important as the CPU, chipsets should be evaluated when reviewing a system. The chipset determines which CPU a system can use, integrated ports the system can provide natively, and the number and types of expansion slots a motherboard can support. If the chipset includes the memory controller, it is responsible for determining what type and speeds of RAM the system can use.

The exact division of tasks  between chipset components varies by chipset, and, on some systems, ports such as USB 3.0 are handled by a separate controller ship. Unlike the CPU, which is removable and upgradable, the shipsets components are surface mounted to the motherboard. The only way to change the chipset is to replace the motherboard.  To protect the chipsets from heat, they are often covered by heat sinks.

Some systems a separate chip called the Super I/O chip is also used to control some slower devices.

### Northbridge
Also referred to as the memory controller hub (MCH) or, on Intel systems with chipset integrated, the graphics memory controller hub (GMCH). This chip connects to the CPU and other high-speed components such as memory, PCIe video (either through expansion slots or integrated into the chipset), USB 3.0 ports, and other high-speed components.

### Southbridge
Also known as the I/O controller hub (ICH). The south bridge chip connects to lower-speed components, such as mass storage interfaces, PCI expansion slots, USB 2.0 ports, audio, and the CMOS.

## CMOS battery
The CMOS chip on the motherboard stores settings configured in the system BIOS. The CMOS battery provides power to maintain the contents of te CMOS chip. A jumper block placed over the 2 jumper pins is how CMOS is cleared on most systems. Some systems feature a port-cluster-mounted push button to clear the CMOS.

A weak CMOS battery will often cause the computers clock to lose time, which it may often only maintain. Having to configure the BIOS after every boot generally points to a bad battery.

On older systems, removing the battery can reset the BIOS configuration.

## Power connections and types
The 24-pin ATX connector provides power to expansion slots and most bus connectors.
The 8-pin EPS12V connector provides a dedicated 12V power for the CPU. The motherboard steps down this voltage to the voltage needed by the installed CPU.Some older boards and boards designed for low-power CPU use a 4 pin ATX12V connector instead of EPS12V.

## Fan connectors
Most motherboards have a CPU fan connector and several system fan connectors (used for fans connected to the case). Both types have a monitor connection to provide fan speed information to the PC health or system monitor feature built into the system BIOS. Some boards have a connector to monitor the speed of the fan built in the power supply.

The CPU fan differs from typical system fan connectos by including a 4th pin used for speed control. System fans with speed control also use a 4 pin connector.

## Front/top panel connectors
Microphone and headphones; found on almost all motherboards.

Music CD playback from optical drives is rarely needed because Windows Media PLayer and other media player programs can play music through the SATA interface and newer optical drives no longer include separate music CD playback connectors. The SPDIF header is designed to support an optional SPDIF bracket for digital audio playback. The bracket is provided by the motherboard vendor but it is not always bundled with compatible motherboards. Front panel audio sometimes has two sets of connectors: one for older AC'97 audio standard and one for HD.

ATX and ITX motherboards include several font/top-panel connectors for the power button, power light, drive activity lights, reset button, USB, and audio case speaker. These connectors are grouped together on or near the front edge of the board. Because front-panel leads are small and difficult to install, some board vendors provide a quick connect extender for easier installation. You would connect the leads to the extender and the extender to the front-panel headers.

### USB

### Audio

### Power button

### Power light

### Drive activity lights

## Bus speeds
The different components of the motherboard, such as CPU, memory, chipset, expansion slots, storage interfaces, and I/O ports, connect with each other at different speeds. The term bus speeds refers to the speeds at which differet buses in the motherboard conencto different components. Some of these speeds, such as the speed of I/O ports and expansion slots (USB, FireWire, SATA; PCI and PCIe slots), are established by the design of the port or by the  capabilities of the devices conencted to them. However, depending on the board, you may be able to fine-tune the bus speeds used by the processor, the chipset interconenct, and memory. The adjustments are generally made through the BIOS, in menus such as Memory, Overclocking, AI Tweaker, and others.

## Reset button
